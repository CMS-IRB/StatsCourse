#!/usr/bin/env python

import csv
import ROOT
import array
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import colors


#methods = ['Fisher']
#methods = ['MLP']
#methods = ['BDT']
methods = ['Fisher', 'MLP', 'BDT']

# ----------------------------------------------------------
def makePlots(sample, method, fvecs_np_train, labels_np_train, fvecs_np_test, labels_np_test, xs, ys, Z, Z_class):

    # plot the training data and Fisher discriminant contour
    plt.contourf(xs, ys, Z, 100, cmap=plt.cm.get_cmap('seismic_r'))
    plt.colorbar()
    plt.scatter([fvecs_np_train[:, 0]], [fvecs_np_train[:, 1]], c=labels_np_train, s=50, cmap=colors.ListedColormap(['red', 'blue']))
    plt.xlabel('x')
    plt.ylabel('y')
    plt.savefig('training_data_%s_%s.pdf'%(sample,method))
    # clear the figure
    plt.clf()

    # plot the training data and Fisher discriminant class contour
    plt.contourf(xs, ys, Z_class, 100, cmap=plt.cm.get_cmap('seismic_r'))
    plt.scatter([fvecs_np_train[:, 0]], [fvecs_np_train[:, 1]], c=labels_np_train, s=50, cmap=colors.ListedColormap(['red', 'blue']))
    plt.scatter([fvecs_np_test[:, 0]], [fvecs_np_test[:, 1]], c=labels_np_test, s=50, cmap=colors.ListedColormap(['green', 'orange']))
    plt.xlabel('x')
    plt.ylabel('y')
    plt.savefig('training_data_%s_%s_classes.pdf'%(sample,method))
    # clear the figure
    plt.clf()

# ----------------------------------------------------------
def processSample(sample, nTrain_Signal=500, nTrain_Background=500, nTest_Signal=100, nTest_Background=100, cut=None):

    # read training data
    csv_file = open('datasets/%s_data_train.csv'%sample, 'rb')
    reader = csv.reader(csv_file)

    # create ROOT ntuple
    ntuple = ROOT.TNtuple("tuple","tuple","x:y:signal")

    # arrays to hold the labels and feature vectors
    labels = []
    fvecs = []

    for i, row in enumerate(reader):
        values = [float(row[1]), float(row[2]), float(row[0])]
        ntuple.Fill(array.array("f",values))
        labels.append(int(row[0]))
        fvecs.append([float(x) for x in row[1:]])

    # convert the array of float arrays into a numpy float matrix
    fvecs_np = np.matrix(fvecs).astype(np.float32)

    # convert the array of int labels into a numpy array
    labels_np = np.array(labels).astype(np.float32)

    # for plotting training data
    fvecs_np_train  = fvecs_np
    labels_np_train = labels_np

    # determine canvas borders for contour plot
    mins = np.amin(fvecs_np,0); 
    mins = mins - 0.1*np.abs(mins);
    maxs = np.amax(fvecs_np,0); 
    maxs = maxs + 0.1*maxs;

    ## generate dense grid and set Z (discriminator) values to zero
    xs,ys = np.meshgrid(np.linspace(mins[0,0],maxs[0,0],300), np.linspace(mins[0,1], maxs[0,1], 300));
    Z = {}
    Z_class = {}

    for m in methods:
        Z[m] = np.zeros((300,300))
        Z_class[m] = np.zeros((300,300))

    # read testing data
    csv_file = open('datasets/%s_data_eval.csv'%sample, 'rb')
    reader = csv.reader(csv_file)

    # arrays to hold the labels and feature vectors for testing data
    labels_test = []
    fvecs_test = []

    for i, row in enumerate(reader):
        values = [float(row[1]), float(row[2]), float(row[0])]
        ntuple.Fill(array.array("f",values))
        labels.append(int(row[0]))
        labels_test.append(int(row[0]))
        fvecs.append([float(x) for x in row[1:]])
        fvecs_test.append([float(x) for x in row[1:]])

    # convert the array of float arrays into a numpy float matrix
    fvecs_np = np.matrix(fvecs).astype(np.float32)
    fvecs_np_test = np.matrix(fvecs_test).astype(np.float32)

    # convert the array of int labels into a numpy array
    labels_np = np.array(labels).astype(np.float32)
    labels_np_test = np.array(labels_test).astype(np.float32)


    ROOT.TMVA.Tools.Instance()

    # note that it seems to be mandatory to have an
    # output file, just passing None to TMVA::Factory(..)
    # does not work. Make sure you don't overwrite an
    # existing file.
    outfname = "TMVA_%s.root"%(sample + ('_All' if len(methods)>1 else '_' + methods[0]))
    # output file
    outputFile = ROOT.TFile( outfname, 'RECREATE' )

    factory = ROOT.TMVA.Factory("TMVAClassification", outputFile,
                                ":".join([
                                    "!V",
                                    "!Silent",
                                    "Color",
                                    "DrawProgressBar",
                                    "Transformations=I;D;P;G,D",
                                    "AnalysisType=Classification"]
                                        ))

    factory.SetVerbose( True )

    dataloader = ROOT.TMVA.DataLoader(sample)

    dataloader.AddVariable("x","F")
    dataloader.AddVariable("y","F")

    dataloader.AddSignalTree(ntuple)
    dataloader.AddBackgroundTree(ntuple)

    # cuts defining the signal and background sample
    sigCut = ROOT.TCut("signal > 0.5")
    bgCut = ROOT.TCut("signal <= 0.5")

    dataloader.PrepareTrainingAndTestTree(sigCut,   # signal events
                                        bgCut,    # background events
                                        ":".join([
                                            "nTrain_Signal=%i"%nTrain_Signal,
                                            "nTrain_Background=%i"%nTrain_Background,
                                            "nTest_Signal=%i"%nTest_Signal,
                                            "nTest_Background=%i"%nTest_Background,
                                            "SplitMode=Block",
                                            "NormMode=NumEvents",
                                            "!V"
                                        ]))

    # book methods
    for m in methods:
        if m == 'Fisher':
            factory.BookMethod( dataloader, ROOT.TMVA.Types.kFisher, "Fisher", "H:!V:Fisher:CreateMVAPdfs:PDFInterpolMVAPdf=Spline2:NbinsMVAPdf=50:NsmoothMVAPdf=10")
        elif m == 'MLP':
            factory.BookMethod( dataloader, ROOT.TMVA.Types.kMLP, "MLP", "H:!V:NeuronType=tanh:VarTransform=N:NCycles=600:HiddenLayers=N+5:TestRate=5:!UseRegulator")
        elif m == 'BDT':
            factory.BookMethod( dataloader, ROOT.TMVA.Types.kBDT, "BDT", "!H:!V:NTrees=500:MinNodeSize=5%:MaxDepth=3:BoostType=AdaBoost:AdaBoostBeta=0.5:SeparationType=GiniIndex:nCuts=20")

    # Train MVAs
    factory.TrainAllMethods()
    # Test MVAs
    factory.TestAllMethods()
    # Evaluate MVAs
    factory.EvaluateAllMethods()

    # Save the output.
    outputFile.Close()

    print "=== wrote root file %s\n" % outfname
    print "=== TMVAClassification is done!\n"

    # initialize the reader
    reader = ROOT.TMVA.Reader()

    varx = array.array('f',[0])
    vary = array.array('f',[0])

    reader.AddVariable("x",varx)
    reader.AddVariable("y",vary)

    for m in methods:
        reader.BookMVA(m, "%s/weights/TMVAClassification_%s.weights.xml"%(sample,m))

    for i in range(300):
        for j in range(300):

            # set the point coordinates
            varx[0] = xs[i,j]
            vary[0] = ys[i,j]

            # calculate the value of the classifier
            # function at the given coordinate
            for m in methods:
                output = reader.EvaluateMVA(m)
                Z[m][i,j] = output
                Z_class[m][i,j] = (1. if output>cut[m] else 0.)

    # plot the training data
    plt.scatter([fvecs_np_train[:, 0]], [fvecs_np_train[:, 1]], c=labels_np_train, s=50, cmap=colors.ListedColormap(['red', 'blue']))
    plt.xlabel('x')
    plt.ylabel('y')
    plt.savefig('training_data_%s.pdf'%sample)
    # clear the figure
    plt.clf()

    # make remaining plots
    for m in methods:
        makePlots(sample, m, fvecs_np_train, labels_np_train, fvecs_np_test, labels_np_test, xs, ys, Z[m], Z_class[m])

# ----------------------------------------------------------
def main():

    cuts = {}
    cuts['Fisher'] = 0.
    cuts['MLP'] = 0.5
    cuts['BDT'] = 0.

    processSample('linear', nTrain_Signal=500, nTrain_Background=500, nTest_Signal=100, nTest_Background=100, cut=cuts)

    processSample('moon', nTrain_Signal=1000, nTrain_Background=1000, nTest_Signal=503, nTest_Background=497, cut=cuts)

    processSample('saturn', nTrain_Signal=264, nTrain_Background=236, nTest_Signal=52, nTest_Background=48, cut=cuts)

# ----------------------------------------------------------
if __name__ == "__main__":
    main()

