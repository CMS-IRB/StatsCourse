#!/usr/bin/env python

import sys
from optparse import OptionParser
import ROOT


# ----------------------------------------------------------
def main():
    # usage description
    usage = "Usage: ./analyzeTraining.py -i input_file.root \nExample: ./analyzeTraining.py -i TMVA.root"

    # input parameters
    parser = OptionParser(usage=usage)

    parser.add_option("-i", "--input", dest="input_file",
                        help="Input file",
                        metavar="INPUT_FILE")

    (options, args) = parser.parse_args()

    # make sure all necessary input parameters are provided
    if not options.input_file:
        print usage
        sys.exit()

    # open the GUI for the result macros
    ROOT.TMVA.TMVAGui(options.input_file)

    # keep the ROOT thread running
    ROOT.gApplication.Run()


# ----------------------------------------------------------
if __name__ == '__main__':
    main()
